import os
import os.path
import pickle
import requests
import time

from datetime import datetime, timedelta, timezone
from dotenv import load_dotenv
from urllib.parse import quote

import discord

from discord.ext import tasks, commands

# Presents details of an item that is being monitored by folks
class MonitoredItem:
    def __init__(self, krono, plat):
        self.krono = krono
        self.plat = plat
        self.subscribed_people = set()
        self.subscribe_time = datetime.now()

    def subscribe_person(self, person):
        self.subscribed_people.add(person)

    def unsubscribe_person(self, person):
        self.subscribed_people.remove(person)

    def can_nuke(self):
        return len(self.subscribed_people) == 0

# Presents details of an item that is being sold from TLP auctions
class ForSaleItem:
    def __init__(self):                
        self.lastId = ""

    @staticmethod
    def get_cost(krono, plat):
        return (krono * 1000000000) + plat

load_dotenv()

# Secret tokens are stored as environment variables
TOKEN = os.getenv('CV_DISCORD_TOKEN')
GUILD_ID = int(os.getenv('CV_GUILD_ID'))
CHANNEL_ID = int(os.getenv('CV_CHANNEL_ID'))
TIME_OFFSET_IN_MIN = int(os.getenv('CV_TIME_OFFSET_IN_MIN')) # e.g., 180 minutes if in PST due to TLP auctions running on EST

# Standard variables
NUMBER_OF_MINS_IN_DAY = 1440
NUMBER_OF_MINS_IN_HOUR = 60
NUMBER_OF_SEC_IN_MIN = 60
MONITOR_FILE_NAME = 'monitoredItems.pickle'
MEMO_FILE_NAME = "itemMemo.pickle"
client = discord.Client()
monitored_items = dict()    #<itemName, MonitoredItem>
item_memo = dict() # <itemName, ForSaleItem>

# Triggered by Discord when the bot is successfully initialized
@client.event
async def on_ready():    
    print(f'{client.user} has connected to Discord!')
    print('Loading monitored items from disk')
    
    if os.path.isfile(MONITOR_FILE_NAME):
        with open(MONITOR_FILE_NAME, 'rb') as handle:
            monitored_items.update(pickle.load(handle))
    
    if os.path.isfile(MEMO_FILE_NAME):
        with open(MEMO_FILE_NAME, 'rb') as handle:
            item_memo.update(pickle.load(handle))

    print(str(len(monitored_items)) + ' monitored items loaded from disk') 
    print(str(len(item_memo)) + ' item memos loaded from disk') 
      
# Triggered by Discord when the bot receives a message from the channel
@client.event
async def on_message(message):
    print('Got a message: ' + message.content)
    if message.author == client.user:
        return

    if message.content.lower().startswith('cv'):
        await on_cv_message(message)

# Invokes when a Cronovega-specific message is received.
# This function will fork to command-specific implementations.
async def on_cv_message(message):
    tokens = message.content.lower().split()
    if len(tokens) == 1:
        return

    command = tokens[1]

    if command in { 'monitor', 'add' }:
        await on_monitor_item(tokens[2:], message)
    elif command in { 'forget', 'remove' }:
        await on_forget_item(tokens[2:], message)
    elif command == 'clear':
        await on_clear(message)
    elif command == 'list':
        await on_list(message)
    elif command == 'help':
        await on_help(message)
    elif command in { 'get', 'check' }:
        await on_check(tokens[2:], message)

# Invoked when requesting to monitor for an item
# Usage: cv montior <item> <maxKrono> <maxPlat>
# Example: cv monitor fungus covered scale tunic 5 5000
async def on_monitor_item(tokens, message):
    person = message.author.name
    item_name = ''
    krono = 0
    plat = 0

    # attempt to parse the price strings into integers
    try:
        krono = int(tokens[-2])
        plat = int(tokens[-1])
        item_name = ' '.join(tokens[:-2])   # join all words except the last two in the array
    except ValueError:
        # if we get an error, we will simply assume they are monitoring it regardless of price.
        print ('Monitoring for any price')
        item_name = ' '.join(tokens)

    # see if the item is currently being monitored
    if item_name in monitored_items:
        monitored_item = monitored_items[item_name]

        # see if the person already subscribed
        if person in monitored_item.subscribed_people:
            await message.channel.send(f'>> {person} is already monitoring the item [{item_name}] <<')
        # subscribe a new person
        else:
            monitored_item.subscribe_person(person)
            await persist_monitors()
   # it's not being monitored, so start monitoring it
    else:
        new_item = MonitoredItem(krono, plat)
        new_item.subscribe_person(person)
        monitored_items[item_name] = new_item
        await persist_monitors()
        if (krono == 0 and plat == 0):
            await message.channel.send(f'>> I will monitor [{item_name}] and notify [{person}] if I find the item for any price! <<')
        else:
            await message.channel.send(f'>> I will monitor [{item_name}] and notify [{person}] if I find it for less than or equal to {krono}kr, {plat}pp! <<')
    return

# Invoked to stop monitoring a specific item for a user
# Usage: cv forget <itemName>
# Example: cv forget fungus covered scale tunic
async def on_forget_item(tokens, message):
    item_name = ' '.join(tokens)
    person = message.author.name
    # See if we're even monitoring this item
    if item_name not in monitored_items:
        await message.channel.send(f'>> Umm... I wasn\'t monitoring item [{item_name}] <<')
    # We are monitoring it, so let's remove the person if they're subscribed
    else:        
        monitored_item = monitored_items[item_name]
        if person in monitored_item.subscribed_people:
            monitored_item.unsubscribe_person(person)            
            await message.channel.send(f'>> No longer monitoring item [{item_name}] for [{person}] <<')

        # If there's nobody subscribed, we can simply remove this item from being monitored
        if monitored_item.can_nuke():
            del monitored_items[item_name]
            item_memo.pop(item_name, None)
        await persist_monitors()
    return

# Invoked to clear all monitors for all users
# Usage: cv clear
async def on_clear(message):
    await message.channel.send('>> Forgetting all ' + str(len(monitored_items)) + ' item monitors <<')
    monitored_items.clear()
    item_memo.clear()
    await persist_monitors()
    return

# Invoked to list all the current item monitors
# Usage: cv list
async def on_list(message):
    await message.channel.send('>> Monitoring ' + str(len(monitored_items)) + ' items: <<')
    for item in monitored_items:
        monitored_item = monitored_items[item]
        to_send = f'{item} '
        if monitored_item.krono == 0 and monitored_item.plat == 0:
            to_send = to_send + '[Any Price]'
        else:
            to_send = to_send + f'[{monitored_item.krono}kr, {monitored_item.plat}pp]'
        to_send = to_send + ' - Subscribers: ' + ', '.join(monitored_item.subscribed_people)
        await message.channel.send(to_send);
    return

# Invoked to provide help output
# Usage: cv help
async def on_help(message):    
    help_syntax = '>> Supported Syntax <<\n'
    help_syntax += '**cv monitor**\n\t*Description*: Adds a monitor for current user for specific item with price range.\n\t*Usage*: cv montior <itemName> <maxKrono> <maxPlat>\n\t*Example*: cv monitor fungus covered scale tunic\n'
    help_syntax += '**cv forget**\n\t*Description*: Stop monitoring an item for current user.\n\t*Usage*: cv forget <itemName>\n\t*Example*: cv forget fungus covered scale tunic\n'    
    help_syntax += '**cv clear**\n\t*Description*: Stop monitoring all items for all users.\n\t*Usage*: cv clear\n'
    help_syntax += '**cv list**\n\t*Description*: Shows all current item monitors for all users.\n\t*Usage*: cv list\n'
    help_syntax += '**cv check**\n\t*Description*: Find all available listings from TLP for specific item\n\t*Usage*: cv check <itemName>\n\t*Example*: cv check fungus covered scale tunic'

    await message.channel.send(help_syntax);
    return

# Helper function to serialize and persist desired objects to file
async def write_to_file(filename, object_to_dump):    
    with open(filename, 'wb') as handle:
        pickle.dump(object_to_dump, handle)

# Invoked when the monitored items gets updated (cleared, updated, removed)
async def persist_monitors():
    await write_to_file(MONITOR_FILE_NAME, monitored_items)

# Invoked when the items memo gets updated (cleared, updated, removed)
async def persist_memo():
    await write_to_file(MEMO_FILE_NAME, item_memo)
   
# Given the input string from a TLP item, return the local time (adjusted via offset)
def get_local_time_from_tlp_date_string(tlp_datetime):
    original_dt = datetime.fromisoformat(tlp_datetime)
    offset_delta = timedelta(minutes=TIME_OFFSET_IN_MIN)
    return original_dt + offset_delta

# Invoked by Discord every X seconds and is used as a long-poll for querying items that are 
# currently being monitored via TLP auctions.
@tasks.loop(seconds = 30)
async def check_monitors():
    print('Checking monitors for ' + str(len(monitored_items)) + ' items')
    updates_made = False

    for item in monitored_items:
        monitored_item = monitored_items[item]
        items = search_tlp(item)        
        
        # Check if we already have a memo entry for the item. If not, create
        if (item not in item_memo):            
            item_memo[item] = ForSaleItem()
            updates_made = True
        
        last_id = item_memo[item].lastId
        print(f'Handling monitor request for item [{item}].  Last ID: {last_id}')
       
        if len(items) > 0:
            # For each item, check if its posted time is within past 35 seconds
            for tlp_item in items:

                # Pull information from the response
                item_id = tlp_item['id']
                krono = tlp_item['krono_price']
                plat = tlp_item['plat_price']
                cost = ForSaleItem.get_cost(krono, plat)      
                seller = tlp_item['auctioneer']
                selling_item_name = tlp_item['item']
                list_time = get_local_time_from_tlp_date_string(tlp_item['datetime'])

                print(f'Analyzing item: [{selling_item_name}] sold by [{seller}] for {krono}kr and {plat}pp')
                  
                # If we've seen this listing before, we can simply exit
                if (item_id == item_memo[item].lastId):
                    print ("We've already seen this item and ID.")
                    break
                # If the returned item was listed before the monitor request was received, we are done
                if (list_time < monitored_item.subscribe_time):
                    print (f'Found item [{selling_item_name}] but was listed prior to the monitor being requested.  Ignoring.')
                    break
                
                cost_ceiling = ForSaleItem.get_cost(monitored_item.krono, monitored_item.plat)
                if (cost_ceiling > 0):                    
                    # Skip 0 cost if we have a cost ceiling
                    if (krono == 0.0 and plat == 0.0):
                        print ('Item has not provided any pricing, ignoring')
                        continue                    
                    # Let's see if it's less than what we're subscribed for
                    if (cost > cost_ceiling):
                        print (f'Found item [{selling_item_name}] but exceeded cost ceiling.  Ceiling = {monitored_item.krono}kr, {monitored_item.plat}pp -- Found: {krono}kr, {plat}pp')
                        continue
                
                # Item exists, cost is below threshold, any ID is new
                updates_made = True                            
                # Send message                    
                ping_list = [f'@{i}' for i in monitored_items[item].subscribed_people]
                ping_string = ', '.join(ping_list)
                channel = client.get_channel(CHANNEL_ID)
                formatted_time = get_formatted_time_ago(list_time)
                await channel.send(f'>> New listing found for item [{selling_item_name}] for {krono}kr, {plat}pp from seller [{seller}] {formatted_time} ago!<<')
                await channel.send(ping_string)
                print(ping_string)

            # Assign the last itemID for the item memo to the latest received from TLP
            item_memo[item].lastId = items[0]['id']
    
    #Save memo
    if updates_made == True:        
        print ("Updated detected - persisting memo")
        await persist_memo()

# Returns a list of items from TLP auctions for the provided search term
def search_tlp(item_name):
    sanitized_item_name = quote(item_name)
    uri = f'https://api.tlp-auctions.com/GetSalesLogs?pageNum=1&pageSize=5&searchTerm={sanitized_item_name}&filter=sell&exact=false&serverName=mischief'
    print(uri)    
    response = requests.get(uri)
    while response.status_code != 200:
        print(f'Got status code: {response.status_code} -- retrying')
        time.sleep(5)
        response = requests.get(uri)
    json = response.json()
    print(json)
    return json['items']

# Offsets any duration in minutes by the system offset.
def get_minutes_ago(duration_in_min):
    return max(0, round(duration_in_min + TIME_OFFSET_IN_MIN))

# Returns string format of days/hours/mins since the provided datetime
def get_formatted_time_ago(list_time):
    now = datetime.now()
    # we need to account for cases in which there's clock skew and a list time is more recent than 'now' on our machine
    delta = timedelta(minutes=0) if list_time > now else now - list_time
    mins_since_list_time = delta.total_seconds() // NUMBER_OF_SEC_IN_MIN

    # Do the math
    days = int(mins_since_list_time // NUMBER_OF_MINS_IN_DAY)
    leftover_minutes = mins_since_list_time % NUMBER_OF_MINS_IN_DAY    
    hours = int(leftover_minutes // NUMBER_OF_MINS_IN_HOUR)
    mins = int(mins_since_list_time - (days*NUMBER_OF_MINS_IN_DAY) - (hours*NUMBER_OF_MINS_IN_HOUR))

    # Format the output
    output_string = []
    if (days > 0):
        output_string.append(f'{days} days')
    
    if (hours > 0):
        output_string.append(f'{hours} hours')
    
    if (mins > 0 or (days == 0 and hours == 0)):
        output_string.append(f'{mins} mins')

    return ', '.join(output_string)

# Invoked to execute a one-off query for an item and return the top N results
# Usage: cv check <itemName>
# Example: cv check fungus covered scale tunic
async def on_check(tokens, message):
    # We need at least one token to do any searches
    if (len(tokens) == 0):
        return

    person = message.author.name
    item_name = ' '.join(tokens)
    items = search_tlp(item_name)
    
    channel = client.get_channel(CHANNEL_ID)
    num_items = len(items)
    await channel.send(f'>> Found {num_items} listings for [{item_name}] <<')
    for item in items:
         # pull information from the response
            krono = item['krono_price']
            plat = item['plat_price']
            seller = item['auctioneer']
            selling_item_name = item['item']
            list_time = get_local_time_from_tlp_date_string(item['datetime'])
            formatted_time = get_formatted_time_ago(list_time)
            await channel.send(f'{selling_item_name} -- Price: {krono}kr, {plat}pp -- Seller: [{seller}] -- When: {formatted_time} ago')

# Beginning of execution
check_monitors.start()
client.run(TOKEN)